#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause


import base64
from datetime import datetime as dt
from os import linesep,path
from sys import path as syspath
import time
from sys import argv,exit

syspath.append(path.join(path.dirname(__file__), "lib"))
try:
    #import prtal
    from prtal import tal5226onetime as sha   # sha mnemonic for sal harness 
except ImportError:
    print(syspath)
    raise

try:
	from tal5226onetime import OnetimeDec,Onetime16,EndFeed
	from onetimethread import OnetimeThread
except ImportError:
	from prtal.tal5226onetime import OnetimeDec,Onetime16,EndFeed
	from prtal.onetimethread import OnetimeThread

try:
	from dechext import DecHext,Dzz,D43zz,CounterPlus,CounterAlpha16
	from listex import Cons,Cons4,Pow16
except ImportError:
	from prtal.dechext import DecHext,Dzz,D43zz,CounterPlus,CounterAlpha16
	from prtal.listex import Cons,Cons4,Pow16


THREE=3		# Because everything in here should be using same threadcount=3, lets have a convenience var
""" Next is the power of the pr sal number we are using as MODULO in operations
SALTY is just a mnemonic for tal number t
"""
SALTY=5226

vermaj=Cons.VER62
vermin=1098

def process_hex(ver_major,ver_minor,hexstring,otk=1415926535897000):
	print("{0} Harness for chunk running version {1}.{2}".format(dt.now().isoformat(),vermaj,vermin))
	#hex0x1='0x504b0304140008080800f9995b530000000000000000000000000b0000005f72656c732f2e72656c73ad924d4b03410c86effd1543eedd6c2b88c8cef622426f22f5078499eceed0ce073369adffde410aba508aa0c7bc79f3f01cd26dcefea04e9c8b8b41c3aa69417130d1ba306a78db3d2f1f60d32fba573e90d44a995c2aaade84a26112498f88c54ceca9343171a89b21664f52c73c6222b3a79171ddb6f7987f32a09f31d5d66ac85bbb02b5fb48fc37367a16b2248426665ea65cafb3382e154e7964d160a379a971f96a34950c785d68fd7ba1380ccef0533447cf41ae79f1593858b6b79528a55b4677ff69'
	ot3 = Onetime16(SALTY,THREE,hexstring)			# Instanciate a Onetime object which will handle our end2end attempt
	print("dec:{0}".format(ot3.inputdec))
	endfeed = ot3.end2end6(otk,ver_major,ver_minor,verbosity=2)		# ver_minor=1098 for our fict1024 work
	# Because verbosity set high ( 2 or greater ), our call returns an EndFeedInsecure that has extra fields we want.
	if endfeed.feedback is not None:
		raise ValueError("end2end6 did not complete as expected. Feedback={0}".format(endfeed.feedback))
	"""
	# Next report on the final result after both outer and inner operations.
	wanmpz = endfeed.wanmpz
	print("wanmpz->{0}".format(wanmpz))
	final64 = endfeed.final64
	print("final64->{0}".format(final64))
	"""
	wandec = endfeed.finaldec
	print("finaldec->{0}".format(wandec))
	print("Outer will now be dealt with starting at final_after_outerdec.")
	# Next line [assert] is your vital check. Make a similar check in any harness you create where demonstrating reversibility is important.
	#assert hex0x_recovered_by_inverting == Cons4.TERMITES61X
	""" So far we have demonstrated that the first part (pre the large matrix) can be inverted taking final_after_outerdec back to input [decimal]
	Next we concentrate on the matrix. We will have supplied it with value final_after_outerdec and a 10 tuple of powers to use.
	We aim to demonstrate that final_after_outerdec equals finaldec*M^-1
	"""
	mat_inverse = int(endfeed.prod16inv)		# M^-1 as an integer. Retrieved from field prod16inv of the EndFeed [subclass] object
	""" Signature for OnetimeThread is (salty, sal, threadcount, j, k, p, s, t, u, v, w, x, z, inputdec=0)
	Typical end2end internal creation: OnetimeThread(OnetimeDec.SALTY,None,self.threadcount,jpow,kpow,ppow,spow,tpow,upow,vpow,wpow,xpow,zpow) as sal3:
	We are only instanciating a OnetimeThread here to make use of ott.modded_product_of_supplied(iterable_given)
	That saves us constructing the pr [sal] number in here to do the modding.
	"""
	list16 = [0] * 16
	ONE=1
	p16zeros = Pow16(list16)
	ott = OnetimeThread(SALTY,None,ONE,p16zeros)		# salty=683,sal=none,threadcount=1,...
	mat_decimal_input_recovered = ott.modded_product_of_supplied([wandec,mat_inverse])
	print("dec_to_ott reconstruction mat_decimal_input_recovered={0}".format(mat_decimal_input_recovered))
	mat_decimal_input_unprotected = ott.unprotectdec(val=mat_decimal_input_recovered,verbosity=0)
	#assert mat_decimal_input_unprotected == final_after_outerdec			# final_after_outerdec alias of dec_to_ott
	""" May have been able to achieve some of above by instead outer_after_inverses = ot3.dec_inverted(final_after_outerdec,outer_inv_list)
	but used an ad hoc instance of OnetimeThread() and worked fine
	"""
	protectdec=endfeed.before16
	ottdec_recovered=OnetimeThread.unprotect_decgiven(protectdec)
	print("Before onetimethread [recovered]={0}".format(ottdec_recovered))
	#print("Outer will now be dealt with starting at final_after_outerdec.")
	#dhr = DecHext(ottdec_recovered)
	dhr = D43zz(ottdec_recovered)
	print("base64str1_after_inverses={0}".format(dhr.base64str1))
	return endfeed.feedback


if __name__ == '__main__':

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)


	# Supply onetime key then hex
	arg1 = '123'
	arg1 = '0x'
	if len(argv) > 1:
		try:
			arg1 = argv[1]
		except:
			arg1 = ''
		if len(argv) > 2:
			try:
				arg2 = argv[2]
			except:
				arg2 = ''


	if len(arg2) > 2 and len(arg1) > 3:
		# Have a valid onetime key and hex0x probably
		otk_int = int(arg1)		# first user supplied arg is otk
		htup = DecHext.tuple0x(arg2)
		hex0x = htup[0]
		feedback = process_hex(ver_major=vermaj,ver_minor=vermin,hexstring=hex0x,otk=otk_int)
		if feedback is not None:
			print("end2end failed ... feedback={0}".format(feedback))

