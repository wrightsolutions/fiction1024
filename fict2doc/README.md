# Texts of [semi]famous fiction - as document odt and docx

Text is obtained from copyright and royalty free sources such as gutenberg

Text used for all examples is the plain text file flatland_a_romance1884edition1pageextract.txt in directory fictabbottflatland/sourctext

## Split - arbitrary - a general approach with more specifics in each of the lower level readme's

Regardless of the which of xxd or hexdump you use, the first thing you will have to do if you want to 
conform to the input decimal range 100 to 1034 is to ...

split the data into pieces

The local implementer should decide on an automated process for split and remote reforming

This automated split/reform is not a hard problem, but does need a plan and probably a document describing how.

For the fict2doc examples in these [sub]directories,  
I will follow methods similar to or inspired by the split of plain text in the directories fictabbottflatland and similar.


## Prefer plain text examples?

See instead the following directories:

+ fictabbottflatland
+ fictdickensgreat

You should probably look at the plain text examples first to follow the method used there as here will be similar.

## Point in time checksum of the document(s)

```
075bda413a91585429a0c515870847922f8c0160  opendocument/flatland_a_romance1884edition1page.odt
3bc84c6fd6927cb991e2e86812e5882fd07c4b94  docx/flatland_a_romance1884edition1page.docx
```

Above are sha1sums which are good enough for a simple verify that your copy has not had newer commits.

Both will be committed next after this update to readme
